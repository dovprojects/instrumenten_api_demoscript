# Purpose
DOV provides webservices to manage wells, filters, instruments and measurements (manual and HF). In order to create all these objects, a sequence of webservices needs to be used, some using XML-format, others using JSON-format REST-endpoints. XML-format files can also be uploaded via the webpages. 

This repository contains a jupyter-notebook, that demonstrates this sequence of webservices, and also how could be combined with manual actions in the webpages (upload, validation, ...). 

# Installation
- install anaconda/miniconda and git
- create a workingfolder: *mkdir this_folder*
- move into the folder: *cd this_folder*
- clone the repository in this folder: *git clone url_of_this_repository .* 
    - use the blue button **clone** to get the url,
    - don't forget the final dot!!
- use the provided yml-file to create a new environment (named 'edov'): *conda create -f environment.yml*
- start the conda environment: *activate edov*
- start the notebook: *jupyter-lab*
